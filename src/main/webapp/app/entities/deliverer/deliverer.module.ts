import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { CoopcycleSharedModule } from 'app/shared/shared.module';
import { DelivererComponent } from './deliverer.component';
import { DelivererDetailComponent } from './deliverer-detail.component';
import { DelivererUpdateComponent } from './deliverer-update.component';
import { DelivererDeleteDialogComponent } from './deliverer-delete-dialog.component';
import { delivererRoute } from './deliverer.route';

import { QRCodeModule } from 'angular2-qrcode';

@NgModule({
  imports: [CoopcycleSharedModule, RouterModule.forChild(delivererRoute), QRCodeModule],
  declarations: [DelivererComponent, DelivererDetailComponent, DelivererUpdateComponent, DelivererDeleteDialogComponent],
  entryComponents: [DelivererDeleteDialogComponent]
})
export class CoopcycleDelivererModule {}
